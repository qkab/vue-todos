import axios from 'axios'
const url = '/todos'

const state = {
    todos: []
}
const getters = {
    allTodos: (state) => state.todos
}
const actions = {
    getTodos: async ({ commit }) => {
        const response = await axios.get(url)
        commit('setTodos', response.data)
    },
    addTodo: async ({ commit }, title) => {
        const response = await axios.post(url, {title, completed: false})
        commit('newTodo', response.data)
    },
    deleteTodo: async ({ commit }, id) => {
        await axios.delete(`${url}/${id}`)
        commit('removeTodo', id)
    },
    filterTodos: async ({ commit }, e) => {
        const limit = parseInt(e.target.options[e.target.options.selectedIndex].innerText)
        const response = await axios.get(`${url}?_limit=${limit}`)
        commit('setTodos', response.data)
    },
    updateTodo: async ({ commit }, updatedTodo) => {
        const response = await axios.put(`${url}/${updatedTodo.id}`, updatedTodo)
        commit('setComplete', response.data)
    }
}
const mutations = {
    setTodos: (state, todos) => state.todos = todos,
    newTodo: (state, todo) => state.todos = [todo, ...state.todos],
    removeTodo: (state, id) => state.todos = state.todos.filter(todo => todo.id !== id),
    setComplete: (state, updatedTodo) => state.todos =  state.todos.map(todo => {
        if (todo.id === updatedTodo.id) {
            return updatedTodo
        }
        return todo
    })
}

export default {
    state,
    getters,
    actions,
    mutations
}